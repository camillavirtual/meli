const path = require('path');
const express = require('express');
const axios = require('axios');
const webpack = require('webpack');
const webpackMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const config = require('./webpack.config.prod.js');

const { NODE_ENV, PORT } = process.env;

const isDeveloping = NODE_ENV !== 'development' || 'homolog';
const port = PORT || 3000;

const app = express();

const author = () => ({
  name: 'Cah',
  lastname: 'Felix'
});

const formattedPrice = (price, currency_id) => {
  const splittedPrice = price.toString().split('.');
  const formattedPrice = {
    currency: currency_id,
    amount: splittedPrice[0],
    decimals: splittedPrice[1] ? splittedPrice[1] : '00'
  };

  return formattedPrice;
};

app.get('/api/items', async (req, res) => {
  try {
    const url = `https://api.mercadolibre.com/sites/MLA/search?q=:${req.query.q}&limit=4`;
    const search = await axios.get(url);
    const filters = search.data.filters.length ? search.data.filters[0].values[0].path_from_root.map(item => ( item.name ) ) : [];
    const items = 
      search.data.results.map(result => ({
        id: result.id,
        title: result.title,
        price: formattedPrice(result.price, result.currency_id),
        picture: result.thumbnail.replace('-I.jpg', '-O.jpg'),
        condition: result.condition,
        free_shipping: result.shipping.free_shipping,
        city: result.address.state_name
      }));

    const api = {
      author: author(),
      categories: filters,
      items
    };
    return res.send(api);

  } catch (err){
    res.status(500).send('Algo deu errado!');
  }
});

app.get('/api/items/:id', (req, res) => {
  const { id } = req.params; 
  const getProductDescription = axios.get(`https://api.mercadolibre.com/items/${id}/description`);
  const getProduct = axios.get(`https://api.mercadolibre.com/items/${id}`);

  try {

    axios.all([getProduct, getProductDescription]).then(
      axios.spread((products, description) => {
        const data1 = products.data;
        const data2 = description.data;

        const item = {
          id: data1.id,
          title: data1.title,
          price: formattedPrice(data1.price, data1.currency_id),
          picture: data1.pictures[0].url,
          condition: data1.condition,
          free_shipping: data1.shipping.free_shipping,
          sold_quantity: data1.sold_quantity,
          description: data2.plain_text
        };

        const api = {
          author: author(),
          item
        };

        return res.send(api);
      })
    );
  } catch (err){
    res.status(500).send('Algo deu errado!');
  }
});

if (isDeveloping) {
  const compiler = webpack(config);
  const middleware = webpackMiddleware(compiler, {
    publicPath: config.output.publicPath,
    stats: {
      colors: true,
      hash: false,
      timings: true,
      chunks: false,
      chunkModules: false,
      modules: false
    }
  });

  app.use(middleware);
  app.use(webpackHotMiddleware(compiler));
  app.get('*', function response(req, res) {
    res.write(
      middleware.fileSystem.readFileSync(
        path.join(__dirname, 'dist/index.html')
      )
    );
    res.end();
  });
} else {
  app.use(express.static(__dirname + '/dist'));
  app.get('*', (req, res) => { 
    res.sendFile(path.join(__dirname, 'dist/index.html'));
  });
}

app.listen(port, err => { 
  if (err) {
    console.log(err);
  }

  console.info('');
  console.info(`==> ✅  Servidor Pronto!`);
  console.info(`==> 🌎  Acesse ou atualize: http://localhost:${port}/`);
  console.info('');
});
