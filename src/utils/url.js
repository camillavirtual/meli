const createHistory = require('history').createBrowserHistory;

export const refreshPage = (timeout = 0) => {
  if (timeout > 0) {
    setTimeout(() => {
      const history = createHistory({ forceRefresh: true });
      history.push();
    }, timeout);
  } else {
    const history = createHistory({ forceRefresh: true });
    history.push();
  }
};

export const goToUrl = (url, timeout = 0) => {
  if (timeout > 0) {
    setTimeout(() => {
      const history = createHistory({ forceRefresh: true });
      history.push(url);
    }, timeout);
  } else {
    const history = createHistory({ forceRefresh: true });
    history.push(url);
  }
};

export const goBack = (fallbackUrl = '/') => {
  const history = createHistory({ forceRefresh: true });
  if (history.length > 1 && document.referrer && document.referrer !== '') {
    history.goBack();
  } else {
    goToUrl(fallbackUrl);
  }
};
