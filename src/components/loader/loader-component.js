import React from 'react';

const Loader = () => (
  <div className='ml-loader'>
    <span className='ml-loader__dot ml-loader__dot_1' />
    <span className='ml-loader__dot ml-loader__dot_2' />
    <span className='ml-loader__dot ml-loader__dot_1' />
    <span className='ml-loader__dot ml-loader__dot_4' />
  </div>
);

export default Loader;
