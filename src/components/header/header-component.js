import React from 'react';
import Search from '../search-bar/search-bar-container';
import logo from '../../assets/images/logo_mercado_livre.png';
import logo2x from '../../assets/images/logo_mercado_livre@2x.png';

const Header = () => (
  <header className='ml-header'>
    <div className='ml-header__container'>
      <a className='' href='/'>
        <img src={logo} srcSet={`${logo2x} 2x`} className='ml-header__logo' alt='Logo Mercado Livre' />
      </a>
      <Search />
    </div>
  </header>
);

export default Header;
