import React, { Component } from 'react';
import searchIcon from '../../assets/images/ic_search.png';
import searchIcon2x from '../../assets/images/ic_search@2x.png';

class Search extends Component {
  render() {  
    const { changeQueryAction, submitQuery, query } = this.props;
    
    return (
      <form className='ml-search-bar'>
        <input
          className='ml-search-bar__input'
          placeholder='O que você está procurando?'
          onChange={changeQueryAction}
        />
        <button type='submit' className='ml-search-bar__btn' onClick={() => submitQuery(query)}>
          <img
            src={searchIcon}
            srcSet={`${searchIcon2x} 2x`}
            className='icon'
            alt='Ícone de busca'
          />
        </button>
      </form>
    );
  }
}
export default Search;
