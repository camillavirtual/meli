import { goToUrl } from "../../utils/url";

export const changeQueryAction = event => ({
  type: "CHANGE_QUERY",
  payload: event.target.value
});

export const submitQuery = query => dispatch => {
  dispatch({ type: "SUBMIT" });
  goToUrl(`/#/items?search=${query}`);
};
