const INITIAL_STATE = {
  query: '',
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'CHANGE_QUERY':
      return { ...state, query: action.payload };
    default:
      return state;
  }};