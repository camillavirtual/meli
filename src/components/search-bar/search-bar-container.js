import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { changeQueryAction, submitQuery } from './search-bar-actions';
import Search from './search-bar-component';

const mapStateToProps = state => ({ 
  query: state.search.query,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  changeQueryAction,
  submitQuery
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Search);

