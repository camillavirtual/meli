import React from 'react';

const Breadcrumb = props => (
  <ul className='ml-breadcrumb'>
    {props.categories.length ? (
      props.categories.map(el => <li key={Math.random()} className='ml-breadcrumb__item'>{el}</li>)
    ) : null }   
  </ul>
);

export default Breadcrumb;
