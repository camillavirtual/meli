import React, { Component } from 'react';
import shipping from '../../assets/images/ic_shipping.png';
import shipping2x from '../../assets/images/ic_shipping@2x.png';

export default class item extends Component {
  render() {
    const { id, title, picture, amount, decimals, free_shipping, city } = this.props;

    return (
      <li className='ml-item'>
        <a className='ml-item__link' href={`/#/items/${id}`}>  
          <figure>
            <img className='ml-item__image' src={picture} alt={title} />
          </figure>
          <div className='ml-item__info'>
            <div className='ml-item__info__header'>
              <h2 className='ml-item__info__price'>
                $ {amount},<sup>{decimals}</sup>
                {free_shipping ? (
                  <img className='ml-item__info__image' src={shipping} srcSet={`${shipping2x} 2x`} alt='shipping' />
                ) : null}
              </h2>
              <span className='ml-item__info__city'>{city}</span>
            </div>
            <p className='ml-item__info__title' >{title}</p>
          </div>
        </a>
      </li>
    );
  }
}
