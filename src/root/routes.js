import React  from 'react';
import { Router, Route, hashHistory } from 'react-router';
import Home from '../page/home/home-component';
import Item from '../page/item/item-container';
import Ops from '../page/ops/ops-component-web';
import SearchResults from '../page/search-results/search-results-container';

export default () => (
  <Router history={hashHistory} onUpdate={() => window.scrollTo(0, 0)}>
    <Route exact path='/' component={Home} />
    <Route exact path='/items' component={SearchResults} />
    <Route exact path='/items/:id' component={Item} />
    <Route path='/*' component={Ops} status={404} />
  </Router>
);
