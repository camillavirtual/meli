import { combineReducers } from 'redux';
import search from '../components/search-bar/search-bar-reducer';
import products from '../page/search-results/search-results-reducer';
import item from '../page/item/item-reducer';

const rootReducer = combineReducers({
  search,
  products,
  item,
});

export default rootReducer;