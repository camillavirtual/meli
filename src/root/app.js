import React, { Component } from 'react';
import _ from 'lodash'; 
import Routes from './routes';
import Header from '../components/header/header-component';
import '../assets/scss/app.scss';

class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <Routes />
      </div>
    );
  }
}

export default App;