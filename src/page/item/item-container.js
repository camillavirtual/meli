import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getProduct } from './item-action';
import Item from './item-component';

const mapStateToProps = state => ({ 
  item: state.item.item.item,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getProduct
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Item);

