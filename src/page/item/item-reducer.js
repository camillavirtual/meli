const INITIAL_STATE = {
  error: null,
  item: {},
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'GET_PRODUCT_SUCCESS':
      return { ...state, item: action.payload };
    case 'GET_PRODUCT_ERROR':
      return { ...state, error: action.payload };
    default:
      return state;
  }
};
