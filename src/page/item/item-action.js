import axios from 'axios';

const getProductSucess = payload => ({
  type: "GET_PRODUCT_SUCCESS",
  payload
});

const getProductError = payload => ({
  type: "GET_PRODUCT_ERROR",
  payload
});

export const getProduct = id => dispatch => {
  axios
    .get(`/api/items/${id}`)
    .then(resp => dispatch(getProductSucess(resp.data)))
    .catch(error => dispatch(getProductError(error)));
};