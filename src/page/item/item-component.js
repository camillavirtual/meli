import React, { Component } from 'react';

class Item extends Component {
  componentWillMount() {
    this.props.getProduct(this.props.params.id);
  }

  render() {
    const {
      title,
      picture,
      description,
      condition,
      sold_quantity,
      price,
    } = this.props.item || {};

    return (
      <div className='ml-container ml-product-item'>
        <div className='ml-product-item__header'>
          <img className='ml-product-item__image' src={picture} alt={title} />

          <div className='ml-product-item__box'>
            <span className='ml-product-item__small'>{condition} - {sold_quantity} vendidos</span>
            <h1 className='ml-product-item__title'>{title}</h1>
            <h2 className='ml-product-item__price'>$ {price ? price.amount : null},<sup>{price ? price.decimals : null}</sup></h2>
            <button className='ml-btn'>Comprar</button>
          </div>
        </div>

        <h2 className='ml-product-item__subtitle'>Descrição do Produto</h2>
        <p className='ml-product-item__description'>{description}</p>
      </div>
    );
  }
}

export default Item;
