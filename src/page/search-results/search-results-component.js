import React, { Component } from 'react';
import Item from '../../components/result-card-item/result-card-item';
import Breadcrumb from '../../components/breadcrumb/breadcrumb-component';
import Loader from '../../components/loader/loader-component';

class SearchResult extends Component {
  componentWillMount() {
    this.props.fetchItems(this.props.location.query.search);
  }
  
  render() {
    const { items, categories, loading } = this.props;

    return (
      <div>
        <Breadcrumb categories={categories} />
       
        {loading ? <Loader /> :
          <div className='ml-container ml-search-results'>
            {items.length ? (
              items.map(item => (
                <Item key={Math.random()}
                  id={item.id}
                  title={item.title}
                  picture={item.picture}
                  city={item.city}
                  amount={item.price.amount}
                  decimals={item.price.decimals}
                  currency={item.currency}
                  free_shipping={item.free_shipping}
                />
              ))
            ) : (
              <p>Nenhum item encontrado para sua pesquisa</p>
            )}
          </div>
        }
      </div>
    );
  }
}

export default SearchResult;
