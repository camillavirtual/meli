import axios from 'axios';

const searchRequested = () => {
  return {
    type: "SEARCH_REQUESTED"
  };
};

const searchSuccess = payload => {
  return {
    type: "SEARCH_SUCCESS",
    payload: payload
  };
};

const searchError = error => {
  return {
    type: "SEARCH_ERROR",
    payload: error
  };
};

export const fetchItems = term => dispatch => {
  dispatch(searchRequested());
  axios
    .get(`/api/items?q=${term}`)
    .then(response => dispatch(searchSuccess(response.data)))
    .catch(error => dispatch(searchError(error)));
};
