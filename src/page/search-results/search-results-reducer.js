const INITIAL_STATE = {
  error: null,
  items: [],
  categories: [],
  loading: true
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'SEARCH_SUCCESS':
      return { ...state, items: action.payload.items, categories: action.payload.categories, loading: false };
    case 'SEARCH_ERROR':
      return { ...state, error: action.payload };
    default:
      return state;
  }
};
