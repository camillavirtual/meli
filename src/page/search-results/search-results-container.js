import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getProductList, fetchItems } from './search-results-actions';
import SearchResults from './search-results-component';

const mapStateToProps = state => ({ 
  items: state.products.items,
  categories: state.products.categories,
  loading: state.products.loading
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getProductList,
  fetchItems
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SearchResults);

