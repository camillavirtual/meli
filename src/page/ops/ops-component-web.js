import React from 'react';

const Ops = () => (
  <div className='ml-container'>
    <h1 className='ml-ops--text'>Ops, Não encontramos a página que está procurando</h1>
    <a className='ml-btn' href='/'>Home</a>
  </div>
);

export default Ops;