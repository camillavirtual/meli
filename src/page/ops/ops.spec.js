import React from 'react';
import { render, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import OpsComponent from './ops-component-web';

configure({ adapter: new Adapter() });

describe('Testes do ops', () => {
  it('Componente ops', () => {
    const component = render(<OpsComponent />);
    const text = 'Ops, Não encontramos a página que está procurando.';

    expect(component.length).toEqual(1);
    expect(component.find('.ml-ops--text').text()).toBe(text);
  });
});