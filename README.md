# Acesse

Projeto hospedado https://test-meli.herokuapp.com/#/

## Pré-requisitos

- [Git](https://git-scm.com/)
- [Node](https://nodejs.org/)
- [Npm](https://www.npmjs.com/)
- [Yarn](https://yarnpkg.com/pt-BR/)
- [Sass](http://sass-lang.com/install)

# Instalação
## Node
```
$ curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.4/install.sh | bash
$ nvm install
$ nvm use
```

## Instalando dependências
```
$ yarn install
```

## Desenvolvendo
Após instalar as dependências para rodar o projeto local execute:

```
$ yarn dev
```

## Desenvolvendo com server
Após instalar as dependências para rodar o projeto local execute:

```
$ yarn server
```

## Deploy
Para gerar o build do front execute:

```
$ yarn build
```

## Rodando o servidor em produção
```
yarn start
```

## API Endpoints
* [GET] /api/items?query=:query
* [GET] /api/items/:id